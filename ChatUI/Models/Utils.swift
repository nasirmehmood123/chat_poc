//
//  Utils.swift
//  ChatUI
//
//  Created by Jawad Ahmed on 12/05/2021.
//

import UIKit

class Utils: NSObject {
    
    /************************************************************************************************************/
    // MARK: - LoadingView Methods
    
    class func showLoadingView(withMessage message: String? = nil) {
        DispatchQueue.main.async {
            let window: UIWindow = (UIApplication.shared.delegate?.window!)!
            var view =  window.viewWithTag(777)
            
            if (view == nil) {
                view = UIView.init(frame: window.bounds)
                
                view!.tag = 777
                view!.backgroundColor = UIColor.init(white: 0, alpha: 0.3)
                
                let activity = UIActivityIndicatorView.init()
                activity.style = .large
                activity.startAnimating()
                activity.center = view!.center
                
                let label = UILabel.init(frame: CGRect.init(x: 10, y: (activity.frame.origin.y+activity.frame.size.height+30), width: ((view?.frame.size.width)! - 20), height: 50))
                label.text = message
                label.textColor = UIColor.white
                label.numberOfLines = 2
                label.textAlignment = .center
                view?.addSubview(label)
                
                view!.addSubview(activity)
                window.addSubview(view!)
            }
        }
    }
    
    
    class func removeLoadingView() {
        DispatchQueue.main.async {
            let window: UIWindow = (UIApplication.shared.delegate?.window!)!
            let view = window.viewWithTag(777)
            view?.removeFromSuperview()
        }
    }
}
