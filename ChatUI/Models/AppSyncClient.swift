//
//  AppSyncClient.swift
//  ChatUI
//
//  Created by Jawad Ahmed on 05/05/2021.
//

import UIKit
import AWSAppSync

class AppSyncClient: NSObject {
    static let shared = AppSyncClient()
    
    var awsClient: AWSAppSyncClient?
    
    
    override private init() {
        super.init()
                
        do {
            // initialize the AppSync client configuration configuration
            let cacheConfiguration = try AWSAppSyncCacheConfiguration()
            let appSyncConfig = try AWSAppSyncClientConfiguration(appSyncServiceConfig: AWSAppSyncServiceConfig(),
                                                                  cacheConfiguration: cacheConfiguration)
            // initialize app sync client
            awsClient = try AWSAppSyncClient(appSyncConfig: appSyncConfig)

            // set id as the cache key for objects
            awsClient?.apolloClient?.cacheKeyForObject = { $0["id"] }

            print("AppSyncClient initialized with cacheConfiguration: \(cacheConfiguration)")
        }
        catch {
            print("Error initializing AppSync client. \(error)")
        }
    }
    
    
    class func getUUID() -> String {
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
}
