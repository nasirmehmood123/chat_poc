//
//  ChatListHeaderTableViewCell.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit
protocol ChatListHeaderTableViewCellDelegate:AnyObject {
    func addButtonTapped(id:Int)
}

class ChatListHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var btnAdd: UIButton!

    var id = -1
    var delegate: ChatListHeaderTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func actionAdd(_ sender: Any){
        self.delegate?.addButtonTapped(id: self.id)
    }
    
}
