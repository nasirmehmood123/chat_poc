//
//  ChatListTableViewCell.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImageView.layer.cornerRadius = self.userImageView.frame.height/2
        self.counterLabel.layer.cornerRadius = self.counterLabel.frame.height/2
        self.counterLabel.layer.masksToBounds = true;

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
