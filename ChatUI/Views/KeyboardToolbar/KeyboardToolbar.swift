//
//  KeyboardToolbar.swift
//  ChatUI
//
//  Created by Jawad Ahmed on 06/05/2021.
//

import UIKit

protocol KeyboardToolbarDelegate: NSObjectProtocol {
    func toolbarDidTapSendButton()
}


class KeyboardToolbar: UIView {
    weak var delegate: KeyboardToolbarDelegate?
    
    
    
    //MARK: - IBActions
    
    @IBAction fileprivate func sendAction() {
        delegate?.toolbarDidTapSendButton()
    }
}



