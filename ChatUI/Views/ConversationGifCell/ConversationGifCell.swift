//
//  ConversationGifCell.swift
//  ChatUI
//
//  Created by Jawad Ahmed on 07/05/2021.
//

import UIKit

class ConversationGifCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var gifImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
