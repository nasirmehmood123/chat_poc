//
//  ConversationViewController.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit
import AWSAppSync
import SwiftyGif

class ConversationViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var messageMainView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var btnSent: UIButton!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageViewBottomConstraint: NSLayoutConstraint!
    
    private var refreshControl = UIRefreshControl()
    private let kWriteMessageText = "Write Message..."
    private var messagesList = [ListMessageTypesQuery.Data.ListMessageType.Item?]()
    private var nextToken: String?
    private var isFetching = false
    private var newMessageWatcher: AWSAppSyncSubscriptionWatcher<OnCreateMessageTypeSubscription>?
    private var deleteMessageWatcher: AWSAppSyncSubscriptionWatcher<OnDeleteMessageTypeSubscription>?
    
    var channel = ListChannelTypesQuery.Data.ListChannelType.Item(id: "", name: "", type: 0, createdAt: 0, uuid: "")
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
        self.addAwsWatchers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)

        fetchMessages()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    func config(){
        leftBarButtonItem()
        self.title = channel.name
        
        self.messageTextView.delegate  = self
        self.tableView.delegate       = self
        self.tableView.dataSource     = self
        self.tableView.register(UINib(nibName: "ConversationTableViewCell", bundle: nil), forCellReuseIdentifier: "ConversationTableViewCell")
        self.tableView.register(UINib(nibName: "ConversationGifCell", bundle: .main), forCellReuseIdentifier: "ConversationGifCell")
        self.tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(pullToRefreshHandler), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        self.adjustTextViewHeight()
        self.messageTextView.text      = kWriteMessageText
        self.messageTextView.textColor = .placeholderText
        
        let toolbar = Bundle.main.loadNibNamed("KeyboardToolbar", owner: self, options: nil)?.first as! KeyboardToolbar
        toolbar.delegate = self
        self.messageTextView.inputAccessoryView = toolbar
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.tableView.addGestureRecognizer(tap)
    }
    
    @objc
    func keyboardWillAppear(notification: NSNotification?) {
        
        guard let keyboardFrame = notification?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardHeight: CGFloat
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
        
        let duration = notification?.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0
        self.messageViewBottomConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
        }
        
        if (self.messagesList.count > 0) {
            self.tableView.scrollToRow(at: IndexPath(row: self.messagesList.count - 1, section: 0), at: .bottom, animated: true)
        }
    }
    
    @objc
    func keyboardWillDisappear(notification: NSNotification?) {
        self.messageViewBottomConstraint.constant = 10
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    @objc private func pullToRefreshHandler() {
        messagesList.removeAll()
        nextToken = nil
        fetchMessages()
    }
    
    
    private func reloadTableView(animated: Bool) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            
            if (self.messagesList.count > 0) {
                self.tableView.scrollToRow(at: IndexPath(row: self.messagesList.count - 1, section: 0), at: .bottom, animated: animated)
            }
        }
    }
}



//MARK: - UITableView Methods

extension ConversationViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let message = messagesList[indexPath.row] else {
            return UITableViewCell()
        }
        
        let createAtDate = Date(timeIntervalSince1970: message.createdAt)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        
        if (message.content.hasPrefix("http://") || message.content.hasPrefix("https://")) && message.content.hasSuffix(".gif") {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationGifCell", for: indexPath) as? ConversationGifCell {
                if let gifURL = URL(string: message.content) {
                    cell.userNameLabel.text = message.uuid == AppSyncClient.getUUID() ? "You" : "Recipient"
                    cell.timeStampLabel.text =  formatter.string(from: createAtDate)
                    cell.gifImageView.setGifFromURL(gifURL, customLoader: UIActivityIndicatorView(style: .medium))
                }
                
                return cell
            }
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationTableViewCell", for: indexPath) as? ConversationTableViewCell{
            cell.userNameLabel.text = message.uuid == AppSyncClient.getUUID() ? "You" : "Recipient"
            cell.timeStampLabel.text =  formatter.string(from: createAtDate)
            cell.messageLabel.text = message.content
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 2 && self.nextToken != nil && !isFetching {
            fetchMessages(shouldScrollToBottom: false)
        }
    }
}



//MARK: - UITextview Delegates

extension ConversationViewController:UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        self.adjustTextViewHeight()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == kWriteMessageText {
            textView.text = nil
            textView.textColor = UIColor.label
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = kWriteMessageText
            textView.textColor = .placeholderText
        }
    }
    
    func adjustTextViewHeight() {
//        let fixedWidth = messageTextView.frame.size.width
//        var newSize = messageTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//        if newSize.height > 150 {
//            newSize.height = 150
//        }
//        self.messageViewHeightConstraint.constant = newSize.height + 45
//        self.textViewHeightConstraint.constant = newSize.height
//        self.view.layoutIfNeeded()
    }
}



//MARK: - KeyboardToolbar Delegates

extension ConversationViewController: KeyboardToolbarDelegate {
    func toolbarDidTapSendButton() {
        if self.messageTextView.text != ""{
            self.createNewMessage(content: messageTextView.text, type: 0)
            self.messageTextView.text.removeAll()
        }
    }
}



//MARK: - API Methods

extension ConversationViewController {
    private func fetchMessages(shouldScrollToBottom: Bool = true) {
        if !refreshControl.isRefreshing {
//            refreshControl.beginRefreshing()
//            tableView.setContentOffset(CGPoint(x: 0, y: -refreshControl.frame.size.height), animated: true)
        }
        
        isFetching = true
        
        let listQuery = ListMessageTypesQuery(channelId: channel.id, limit: 20, nextToken: nextToken)
        AppSyncClient.shared.awsClient?.fetch(query: listQuery, cachePolicy: .fetchIgnoringCacheData) { result, error in
            self.isFetching = false
            
            if let error = error {
                print("Error fetching data: \(error)")
                return
            }

            if let errors = result?.errors {
                print("Error in Result: ", errors.first as Any)
                return
            }
            
            if let newItems = result?.data?.listMessageTypes?.items {
                self.messagesList.insert(contentsOf: newItems.reversed(), at: 0)
            }
            
            self.nextToken = result?.data?.listMessageTypes?.nextToken
            
            DispatchQueue.main.async {
                if shouldScrollToBottom {
                    self.reloadTableView(animated: false)
                }
                else {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    private func createNewMessage(content: String, type: Int) {
        let createdAt = Date().timeIntervalSince1970
        let myUUID = AppSyncClient.getUUID()
        let messageMutation = CreateMessageTypeMutation(input: CreateMessageTypeInput(channelId: channel.id, content: content, type: type, createdAt: createdAt, uuid: myUUID))
        let optimisticUpdate: OptimisticResponseBlock = { transaction in
            let newMessage = ListMessageTypesQuery.Data.ListMessageType.Item(id: "temp-id", channelId: self.channel.id, content: content, type: type, createdAt: createdAt, uuid: myUUID)
            self.messagesList.append(newMessage)
            self.reloadTableView(animated: true)
        }
        
        AppSyncClient.shared.awsClient?.perform(mutation: messageMutation, optimisticUpdate: optimisticUpdate, resultHandler: { (result, error) in
            if (error != nil) {
                print("Error: ", error!)
            }
            
            if let snapshot = result?.data?.createMessageType?.snapshot {
                let newMessage = ListMessageTypesQuery.Data.ListMessageType.Item(snapshot: snapshot)
                
                for i in stride(from: self.messagesList.count - 1, to: 0, by: -1) {
                    if var message = self.messagesList[i] {
                        if message.uuid == myUUID && message.createdAt == createdAt {
                            message.id = newMessage.id
                            self.messagesList[i] = message
                        }
                        
                        break
                    }
                }
            }
        })
    }
    
    
    private func addAwsWatchers() {
        let subscriptionRequest = OnCreateMessageTypeSubscription()
        do {
            newMessageWatcher = try AppSyncClient.shared.awsClient?.subscribe(subscription: subscriptionRequest, resultHandler: { [weak self] result, transaction, error in
                guard let self = self else {
                    print("Self is deallocated...")
                    return
                }

                guard let graphQLResponseData = result?.data?.onCreateMessageType else {
                    print("GraphQL response data unexpectedly nil in create channel subscription")
                    return
                }
                
                if graphQLResponseData.uuid == AppSyncClient.getUUID() {
                    return
                }
                
                let newMessage = ListMessageTypesQuery.Data.ListMessageType.Item(snapshot: graphQLResponseData.snapshot)
                self.messagesList.append(newMessage)
                self.reloadTableView(animated: true)
            })
        }
        catch {
            print("Error on Adding Create Channel Subscription ", error)
        }
    }
}
