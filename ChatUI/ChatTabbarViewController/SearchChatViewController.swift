//
//  SearchChatViewController.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit

class SearchChatViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
    }
    
    func config(){
        self.searchTextField.delegate = self
        self.searchTextField.returnKeyType = .search
        self.tableView.delegate       = self
        self.tableView.dataSource     = self
        self.tableView.register(UINib(nibName: "BrowseByTableViewCell", bundle: nil), forCellReuseIdentifier: "BrowseByTableViewCell")


    }
}

extension SearchChatViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseByTableViewCell", for: indexPath) as? BrowseByTableViewCell{
                cell.selectionStyle = .none
                if indexPath.row == 0{
                    cell.titleLabel.text = "Browse People"
                    cell.dividerView.isHidden = true
                }else{
                    cell.titleLabel.text = "Browse Channels"
                    cell.dividerView.isHidden = false
                }
                return cell
            }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Search TextFields Delegate and Search button action
extension SearchChatViewController: UITextFieldDelegate{
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
