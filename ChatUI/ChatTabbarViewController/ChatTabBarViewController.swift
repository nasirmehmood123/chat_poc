//
//  ChatTabBarViewController.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit

class ChatTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
extension UIViewController{
    @objc func leftBarButtonItem(isViewControllerPresented: Bool = false) {
        // hide default navigation bar button item
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = true;
        let buttonBack: UIButton = UIButton(type: .custom) as UIButton
        buttonBack.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        buttonBack.setImage(UIImage(systemName: "arrow.backward"), for: UIControl.State())
        
        if isViewControllerPresented {
            buttonBack.addTarget(self, action:#selector(backButtonClickedForPresentedViewController(_:)), for: UIControl.Event.touchUpInside)
        }
        else {
            buttonBack.addTarget(self, action:#selector(self.leftNavButtonClick(_:)), for: UIControl.Event.touchUpInside)
        }
        
        buttonBack.transform = CGAffineTransform(translationX: -10, y: 0)
        // add the button to a container, otherwise the transform will be ignored
        let suggestButtonContainer = UIView(frame: buttonBack.frame)
        suggestButtonContainer.addSubview(buttonBack)
        let suggestButtonItem = UIBarButtonItem(customView: suggestButtonContainer)
        self.navigationItem.setLeftBarButton(suggestButtonItem, animated: false);
    }
    
    
    @objc func leftNavButtonClick(_ sender:UIButton!)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func backButtonClickedForPresentedViewController(_ sender: UIButton!) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
