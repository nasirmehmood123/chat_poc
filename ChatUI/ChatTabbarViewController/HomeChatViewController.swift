//
//  HomeChatViewController.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit
import AWSAppSync

class HomeChatViewController: UIViewController {

    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var btnCreateMessage:UIButton!

    private var unreadList = [Any]()
    private var directMessagesList = [Any]()
    private var channelsList = [ListChannelTypesQuery.Data.ListChannelType.Item?]()
    private var newChannelWatcher: AWSAppSyncSubscriptionWatcher<OnCreateChannelTypeSubscription>?
    private var deleteChannelWatcher: AWSAppSyncSubscriptionWatcher<OnDeleteChannelTypeSubscription>?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
        self.addAwsWatchers()
    }
    
    func config(){
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.btnCreateMessage.layer.cornerRadius = self.btnCreateMessage.frame.height/2
        
        self.tableView.register(UINib(nibName: "ChatListTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatListTableViewCell")
        self.tableView.register(UINib(nibName: "ChatListHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatListHeaderTableViewCell")
        self.tableView.tableFooterView = UIView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.unreadList.removeAll()
        self.fetchChannelsList(shouldReset: true)
    }
    
    @IBAction func actionWriteNewMessage(_ sender: Any) {
        let vc = NewMessageViewController.instantiate(fromAppStoryboard: .Main)
        self.present(vc, animated: true)

    }
    
    @IBAction func actionAddUnread(_ sender: Any) {
        unreadList.append("-")
        self.tableView.reloadData()
    }
}

extension HomeChatViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return unreadList.count
        }else if section == 1{
            print("numberOfRows: ", channelsList.count)
            return channelsList.count
        }
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "ChatListHeaderTableViewCell") as! ChatListHeaderTableViewCell
        if tableView.numberOfSections == 2{
            if section == 0{
                header.titleLabel.text = "Channels"
                header.id = 1
            }else{
                header.titleLabel.text = "Direct Messages"
                header.id = 2
            }
        }else{
            if section == 0{
                if unreadList.count == 0 {
                    return nil
                }
                
                header.titleLabel.text = "Unread"
                header.id = 0
                header.btnAdd.isHidden = true
            }else if section == 1{
                header.titleLabel.text = "Channels"
                header.id = 1
            }else{
                header.titleLabel.text = "Direct Messages"
                header.id = 2
            }
        }
        header.delegate = self
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 && unreadList.count == 0 {
            return CGFloat.leastNonzeroMagnitude
        }
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListTableViewCell", for: indexPath) as? ChatListTableViewCell{
            cell.selectionStyle = .none
            cell.userNameLabel.text = "Username :\(indexPath.row)"
            
            if indexPath.section == 0 {
                cell.counterLabel.isHidden = false
            }else{
                cell.counterLabel.isHidden = true
            }
            
            if indexPath.section == 1 {
                let channel = channelsList[indexPath.row]
                cell.userNameLabel.text = channel?.name
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            guard let channel = channelsList[indexPath.row] else {
                return
            }
            
            let vc = ConversationViewController.instantiate(fromAppStoryboard: .Main)
            vc.channel = channel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return (indexPath.section == 1)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let channel = channelsList[indexPath.row] else {
                return
            }
            
            deleteChannel(id: channel.id) {
                self.channelsList.remove(at: indexPath.row)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension HomeChatViewController: ChatListHeaderTableViewCellDelegate{
    func addButtonTapped(id: Int) {
        if id == 1{
            print("new channel")
            let vc = NewChannelsViewController.instantiate(fromAppStoryboard: .Main)
            let navVC = UINavigationController(rootViewController: vc)
            navVC.modalPresentationStyle = .fullScreen
            self.present(navVC, animated: true)
        }else{
            print("new direct message")
            let vc = NewMessageViewController.instantiate(fromAppStoryboard: .Main)
            self.present(vc, animated: true)
        }
    }
}



//MARK: - AWS Methods

extension HomeChatViewController {
    private func fetchChannelsList(shouldReset: Bool = false) {
        let listQuery = ListChannelTypesQuery()
        
        AppSyncClient.shared.awsClient?.fetch(query: listQuery, cachePolicy: .fetchIgnoringCacheData) { result, error in
            if let error = error {
                print("Error fetching data: \(error)")
                return
            }

            if let errors = result?.errors {
                print("Error in Result: ", errors.first as Any)
                return
            }

            if (shouldReset) {
                self.channelsList.removeAll()
            }
            
            // Remove existing records if we're either loading from cache, or loading fresh (e.g., from a refresh)
            let newItems = result?.data?.listChannelTypes?.items
            self.channelsList.append(contentsOf: newItems ?? [])
            self.channelsList = self.channelsList.sorted(by: { $0!.createdAt < $1!.createdAt })
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    private func deleteChannel(id: String, completion: @escaping (() -> Void)) {
        let deleteMutation = DeleteChannelTypeMutation(input: DeleteChannelTypeInput(id: GraphQLID(id)))
        AppSyncClient.shared.awsClient?.perform(mutation: deleteMutation, optimisticUpdate: nil, resultHandler: { result, error in
            if let error = error {
                print("Error deleting data: \(error)")
                return
            }

            if let errors = result?.errors {
                print("Error in Result: ", errors.first as Any)
                return
            }
            
            completion()
        })
    }
    
    
    private func addAwsWatchers() {
        let createChannelSubscription = OnCreateChannelTypeSubscription()
        let deleteChannelSubscription = OnDeleteChannelTypeSubscription()
        
        do {
            newChannelWatcher = try AppSyncClient.shared.awsClient?.subscribe(subscription: createChannelSubscription, resultHandler: { [weak self] result, transaction, error in
                guard let self = self else {
                    print("Self is deallocated...")
                    return
                }

                guard let graphQLResponseData = result?.data?.onCreateChannelType else {
                    print("GraphQL response data unexpectedly nil in create channel subscription")
                    return
                }
                
                if graphQLResponseData.uuid == AppSyncClient.getUUID() {
                    return
                }
                
                let newChannel = ListChannelTypesQuery.Data.ListChannelType.Item(snapshot: graphQLResponseData.snapshot)
                self.channelsList.append(newChannel)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            })
            
            deleteChannelWatcher = try AppSyncClient.shared.awsClient?.subscribe(subscription: deleteChannelSubscription, resultHandler: { [weak self] result, transaction, error in
                guard let self = self else {
                    print("Self is deallocated...")
                    return
                }

                guard let graphQLResponseData = result?.data?.onDeleteChannelType else {
                    print("GraphQL response data unexpectedly nil in create channel subscription")
                    return
                }
                
                if graphQLResponseData.uuid == AppSyncClient.getUUID() {
                    return
                }
                
                let newChannel = ListChannelTypesQuery.Data.ListChannelType.Item(snapshot: graphQLResponseData.snapshot)
                for (i, channel) in self.channelsList.enumerated() {
                    if channel?.id == newChannel.id {
                        self.channelsList.remove(at: i)
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            })
        }
        catch {
            print("Error on Adding Create Channel Subscription ", error)
        }
    }
}
