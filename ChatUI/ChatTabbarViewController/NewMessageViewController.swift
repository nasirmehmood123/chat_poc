//
//  NewMessageViewController.swift
//  ChatUI
//
//  Created by Zohaib on 03/05/2021.
//

import UIKit

class NewMessageViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var messageMainView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var btnSent: UIButton!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewOptionsStackView: UIStackView!
    @IBOutlet weak var attachmentStackView: UIStackView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
    }
    
    func config(){
        self.leftBarButtonItem()
        self.title = "New Message"
        self.textViewOptionsStackView.isHidden = true
        self.attachmentStackView.isHidden = true
        self.messageViewHeightConstraint.constant = 50
        self.searchTextField.delegate = self
        self.searchTextField.returnKeyType = .search
        self.messageTextView.delegate = self
        self.tableView.delegate       = self
        self.tableView.dataSource     = self
        self.tableView.register(UINib(nibName: "BrowseByTableViewCell", bundle: nil), forCellReuseIdentifier: "BrowseByTableViewCell")
        self.messageTextView.text      = "Write message..."
        self.messageTextView.textColor = UIColor.lightGray
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    @objc
    func keyboardWillAppear(notification: NSNotification?) {
        
        guard let keyboardFrame = notification?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardHeight: CGFloat
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
        self.messageViewBottomConstraint.constant = keyboardHeight+25
    }
    
    @objc
    func keyboardWillDisappear(notification: NSNotification?) {
        self.messageViewBottomConstraint.constant = 10
    }
    
    @IBAction func actionSend(_ sender: Any){
        if self.messageTextView.text != ""{
            self.messageTextView.text.removeAll()
        }
    }
}

extension NewMessageViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseByTableViewCell", for: indexPath) as? BrowseByTableViewCell{
                cell.selectionStyle = .none
                return cell
            }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Search TextFields Delegate and Search button action
extension NewMessageViewController: UITextFieldDelegate{
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension NewMessageViewController:UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        self.adjustTextViewHeight()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
            self.showOrHideViews(isShow:true)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write message..."
            textView.textColor = UIColor.lightGray
            self.showOrHideViews(isShow:false)
        }
    }
    
    func showOrHideViews(isShow:Bool){
        self.messageViewHeightConstraint.constant = isShow ? 90 : 50

        UIView.transition(with: self.textViewOptionsStackView, duration: 1.0, options: .transitionCrossDissolve) {
            self.textViewOptionsStackView.isHidden = isShow ? false : true
        }
        UIView.transition(with: self.attachmentStackView, duration: 1.0, options: .transitionCrossDissolve) {
            self.attachmentStackView.isHidden = isShow ? false : true
        }
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func adjustTextViewHeight() {
        print("hello")
        let fixedWidth = messageTextView.frame.size.width
        var newSize = messageTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        if newSize.height > 150 {
            newSize.height = 150
        }
        self.messageViewHeightConstraint.constant = newSize.height + 45
        self.textViewHeightConstraint.constant = newSize.height
        self.view.layoutIfNeeded()
    }
}
