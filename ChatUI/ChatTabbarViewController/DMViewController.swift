//
//  DMViewController.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit

class DMViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
    }
    
    func config(){
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "DMTableViewCell", bundle: nil), forCellReuseIdentifier: "DMTableViewCell")
    }
}

extension DMViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DMTableViewCell", for: indexPath) as? DMTableViewCell{
            cell.selectionStyle = .none
            cell.userNameLabel.text = "Username"
            if indexPath.row%2 == 0{
                cell.messageLabel.text = "One line text message show"
            }else{
                cell.messageLabel.text = "More than one or two line text message text shows as it"
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = ConversationViewController.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)

    }

}

