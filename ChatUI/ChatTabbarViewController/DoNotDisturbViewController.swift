//
//  DoNotDisturbViewController.swift
//  ChatUI
//
//  Created by Zohaib on 04/05/2021.
//

import UIKit

class DoNotDisturbViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
    }
    
    func config(){
        self.tableView.delegate       = self
        self.tableView.dataSource     = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.tableFooterView = UIView.init()
    }
}

extension DoNotDisturbViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1{
            return 40
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if indexPath.section == 0{
            cell.textLabel?.text = "\(30+(indexPath.row*5)) minutes"
        }else{
            cell.textLabel?.text = "Custom"
        }
        return cell
    }
    
    
}
