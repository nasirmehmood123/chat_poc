//
//  NewChannelsViewController.swift
//  ChatUI
//
//  Created by Zohaib on 03/05/2021.
//

import UIKit

class NewChannelsViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
    }
    func config(){
        leftBarButtonItem(isViewControllerPresented: true)
        self.title = "Channels"
        self.searchTextField.delegate = self
        self.searchTextField.returnKeyType = .search
        self.tableView.delegate       = self
        self.tableView.dataSource     = self
        self.tableView.register(UINib(nibName: "BrowseByTableViewCell", bundle: nil), forCellReuseIdentifier: "BrowseByTableViewCell")
    }
    
    @IBAction func actionCreateNew(_ sender: Any) {
        let vc = CreateNewChannelTableViewController.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}



extension NewChannelsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "BrowseByTableViewCell", for: indexPath) as? BrowseByTableViewCell{
                cell.selectionStyle = .none
                return cell
            }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        let vc = ConversationViewController.instantiate(fromAppStoryboard: .Main)
//        self.navigationController?.pushViewController(vc, animated: true)

    }
}

// MARK: - Search TextFields Delegate and Search button action
extension NewChannelsViewController: UITextFieldDelegate{
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
