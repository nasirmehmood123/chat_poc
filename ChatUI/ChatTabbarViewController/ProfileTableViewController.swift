//
//  ProfileTableViewController.swift
//  ChatUI
//
//  Created by Zohaib on 03/05/2021.
//

import UIKit

class ProfileTableViewController: UITableViewController {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var statusTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension ProfileTableViewController{
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 1{
            print("Do not disturb")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoNotDisturbNVC")
            self.present(vc!, animated: true)

        }else if indexPath.row == 2{
            print("Set yourself away")
        }else if indexPath.row == 3{
            print("Saved Item")
        }else if indexPath.row == 4{
            print("View profile")
        }else if indexPath.row == 5{
            print("Notifications")
        }else if indexPath.row == 6{
            print("Preferences")
        }
    }
}
