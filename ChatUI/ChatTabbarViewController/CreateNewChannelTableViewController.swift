//
//  CreateNewChannelTableViewController.swift
//  ChatUI
//
//  Created by Zohaib on 03/05/2021.
//

import UIKit
import AWSAppSync

class CreateNewChannelTableViewController: UITableViewController {
    @IBOutlet weak var btnCreateNew: UIBarButtonItem!
    @IBOutlet weak var channelNameTextField: UITextField!
    @IBOutlet weak var channelDescriptionTextView: UITextView!
    @IBOutlet weak var makePrivateSwitch: UISwitch!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.config()
    }
    
    func config(){
        self.leftBarButtonItem()
        self.btnCreateNew.isEnabled = false
        self.channelDescriptionTextView.delegate = self
        self.channelNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.channelDescriptionTextView.text      = "What's this channel about?"
        self.channelDescriptionTextView.textColor = .lightGray
        self.tableView.tableFooterView = UIView.init()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.tableView.addGestureRecognizer(tap)

    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text == ""{
            self.btnCreateNew.isEnabled = false
        }else{
            self.btnCreateNew.isEnabled = true
        }
    }
    
    @IBAction func actionCreateNew(_ sender: Any) {
        createNewChannel(name: channelNameTextField.text!)
    }
    
    
    @IBAction func actionSwitchChanged(_ sender: Any) {
    }

}

extension CreateNewChannelTableViewController:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write message..."
            textView.textColor = UIColor.lightGray
        }
    }
}



//MARK: - API Methods

extension CreateNewChannelTableViewController {
    private func createNewChannel(name: String) {
        Utils.showLoadingView()
        let channelMutation = CreateChannelTypeMutation(input: CreateChannelTypeInput(name: name, type: 1, createdAt: Date().timeIntervalSince1970, uuid: AppSyncClient.getUUID()))
        let optimisticUpdate: OptimisticResponseBlock = { transaction in
            
        }

        AppSyncClient.shared.awsClient?.perform(mutation: channelMutation, optimisticUpdate: optimisticUpdate, resultHandler: { (result, error) in
            Utils.removeLoadingView()
            
            if (error != nil) {
                print("Error: ", error!)
            }

            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Success", message: "Channel Created successfully", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
}
