//
//  MainViewController.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func actionOpenChat(_ sender: Any){
        let vc = ChatTabBarViewController.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)

    }
}

