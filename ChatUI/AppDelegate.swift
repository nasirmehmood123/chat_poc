//
//  AppDelegate.swift
//  ChatUI
//
//  Created by Zohaib on 30/04/2021.
//

import UIKit
import AWSAppSync

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
      
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        _ = AppSyncClient.shared
        return true
    }
}

