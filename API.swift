//  This file was automatically generated and should not be edited.

import AWSAppSync

public struct CreateMessageTypeInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(channelId: GraphQLID, content: String, type: Int? = nil, createdAt: Double, uuid: String) {
    graphQLMap = ["channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid]
  }

  public var channelId: GraphQLID {
    get {
      return graphQLMap["channelId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "channelId")
    }
  }

  public var content: String {
    get {
      return graphQLMap["content"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "content")
    }
  }

  public var type: Int? {
    get {
      return graphQLMap["type"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }

  public var createdAt: Double {
    get {
      return graphQLMap["createdAt"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public var uuid: String {
    get {
      return graphQLMap["uuid"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "uuid")
    }
  }
}

public struct UpdateMessageTypeInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID, channelId: GraphQLID, content: String? = nil, type: Int? = nil, createdAt: Double? = nil, uuid: String? = nil) {
    graphQLMap = ["id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var channelId: GraphQLID {
    get {
      return graphQLMap["channelId"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "channelId")
    }
  }

  public var content: String? {
    get {
      return graphQLMap["content"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "content")
    }
  }

  public var type: Int? {
    get {
      return graphQLMap["type"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }

  public var createdAt: Double? {
    get {
      return graphQLMap["createdAt"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public var uuid: String? {
    get {
      return graphQLMap["uuid"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "uuid")
    }
  }
}

public struct DeleteMessageTypeInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct CreateChannelTypeInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(name: String, type: Int? = nil, createdAt: Double, uuid: String) {
    graphQLMap = ["name": name, "type": type, "createdAt": createdAt, "uuid": uuid]
  }

  public var name: String {
    get {
      return graphQLMap["name"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var type: Int? {
    get {
      return graphQLMap["type"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }

  public var createdAt: Double {
    get {
      return graphQLMap["createdAt"] as! Double
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public var uuid: String {
    get {
      return graphQLMap["uuid"] as! String
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "uuid")
    }
  }
}

public struct UpdateChannelTypeInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID, name: String? = nil, type: Int? = nil, createdAt: Double? = nil, uuid: String? = nil) {
    graphQLMap = ["id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var name: String? {
    get {
      return graphQLMap["name"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var type: Int? {
    get {
      return graphQLMap["type"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }

  public var createdAt: Double? {
    get {
      return graphQLMap["createdAt"] as! Double?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "createdAt")
    }
  }

  public var uuid: String? {
    get {
      return graphQLMap["uuid"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "uuid")
    }
  }
}

public struct DeleteChannelTypeInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: GraphQLID) {
    graphQLMap = ["id": id]
  }

  public var id: GraphQLID {
    get {
      return graphQLMap["id"] as! GraphQLID
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }
}

public struct TableMessageTypeFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: TableIDFilterInput? = nil, channelId: TableIDFilterInput? = nil, content: TableStringFilterInput? = nil, type: TableIntFilterInput? = nil) {
    graphQLMap = ["id": id, "channelId": channelId, "content": content, "type": type]
  }

  public var id: TableIDFilterInput? {
    get {
      return graphQLMap["id"] as! TableIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var channelId: TableIDFilterInput? {
    get {
      return graphQLMap["channelId"] as! TableIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "channelId")
    }
  }

  public var content: TableStringFilterInput? {
    get {
      return graphQLMap["content"] as! TableStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "content")
    }
  }

  public var type: TableIntFilterInput? {
    get {
      return graphQLMap["type"] as! TableIntFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }
}

public struct TableIDFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: GraphQLID? = nil, eq: GraphQLID? = nil, le: GraphQLID? = nil, lt: GraphQLID? = nil, ge: GraphQLID? = nil, gt: GraphQLID? = nil, contains: GraphQLID? = nil, notContains: GraphQLID? = nil, between: [GraphQLID?]? = nil, beginsWith: GraphQLID? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: GraphQLID? {
    get {
      return graphQLMap["ne"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: GraphQLID? {
    get {
      return graphQLMap["eq"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: GraphQLID? {
    get {
      return graphQLMap["le"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: GraphQLID? {
    get {
      return graphQLMap["lt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: GraphQLID? {
    get {
      return graphQLMap["ge"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: GraphQLID? {
    get {
      return graphQLMap["gt"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: GraphQLID? {
    get {
      return graphQLMap["contains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: GraphQLID? {
    get {
      return graphQLMap["notContains"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [GraphQLID?]? {
    get {
      return graphQLMap["between"] as! [GraphQLID?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: GraphQLID? {
    get {
      return graphQLMap["beginsWith"] as! GraphQLID?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct TableStringFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: String? = nil, eq: String? = nil, le: String? = nil, lt: String? = nil, ge: String? = nil, gt: String? = nil, contains: String? = nil, notContains: String? = nil, between: [String?]? = nil, beginsWith: String? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between, "beginsWith": beginsWith]
  }

  public var ne: String? {
    get {
      return graphQLMap["ne"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: String? {
    get {
      return graphQLMap["eq"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: String? {
    get {
      return graphQLMap["le"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: String? {
    get {
      return graphQLMap["lt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: String? {
    get {
      return graphQLMap["ge"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: String? {
    get {
      return graphQLMap["gt"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: String? {
    get {
      return graphQLMap["contains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: String? {
    get {
      return graphQLMap["notContains"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [String?]? {
    get {
      return graphQLMap["between"] as! [String?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }

  public var beginsWith: String? {
    get {
      return graphQLMap["beginsWith"] as! String?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "beginsWith")
    }
  }
}

public struct TableIntFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(ne: Int? = nil, eq: Int? = nil, le: Int? = nil, lt: Int? = nil, ge: Int? = nil, gt: Int? = nil, contains: Int? = nil, notContains: Int? = nil, between: [Int?]? = nil) {
    graphQLMap = ["ne": ne, "eq": eq, "le": le, "lt": lt, "ge": ge, "gt": gt, "contains": contains, "notContains": notContains, "between": between]
  }

  public var ne: Int? {
    get {
      return graphQLMap["ne"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ne")
    }
  }

  public var eq: Int? {
    get {
      return graphQLMap["eq"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "eq")
    }
  }

  public var le: Int? {
    get {
      return graphQLMap["le"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "le")
    }
  }

  public var lt: Int? {
    get {
      return graphQLMap["lt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "lt")
    }
  }

  public var ge: Int? {
    get {
      return graphQLMap["ge"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "ge")
    }
  }

  public var gt: Int? {
    get {
      return graphQLMap["gt"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "gt")
    }
  }

  public var contains: Int? {
    get {
      return graphQLMap["contains"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "contains")
    }
  }

  public var notContains: Int? {
    get {
      return graphQLMap["notContains"] as! Int?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "notContains")
    }
  }

  public var between: [Int?]? {
    get {
      return graphQLMap["between"] as! [Int?]?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "between")
    }
  }
}

public struct TableChannelTypeFilterInput: GraphQLMapConvertible {
  public var graphQLMap: GraphQLMap

  public init(id: TableIDFilterInput? = nil, name: TableStringFilterInput? = nil, type: TableIntFilterInput? = nil) {
    graphQLMap = ["id": id, "name": name, "type": type]
  }

  public var id: TableIDFilterInput? {
    get {
      return graphQLMap["id"] as! TableIDFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "id")
    }
  }

  public var name: TableStringFilterInput? {
    get {
      return graphQLMap["name"] as! TableStringFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "name")
    }
  }

  public var type: TableIntFilterInput? {
    get {
      return graphQLMap["type"] as! TableIntFilterInput?
    }
    set {
      graphQLMap.updateValue(newValue, forKey: "type")
    }
  }
}

public final class CreateMessageTypeMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateMessageType($input: CreateMessageTypeInput!) {\n  createMessageType(input: $input) {\n    __typename\n    id\n    channelId\n    content\n    type\n    createdAt\n    uuid\n  }\n}"

  public var input: CreateMessageTypeInput

  public init(input: CreateMessageTypeInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createMessageType", arguments: ["input": GraphQLVariable("input")], type: .object(CreateMessageType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createMessageType: CreateMessageType? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createMessageType": createMessageType.flatMap { $0.snapshot }])
    }

    public var createMessageType: CreateMessageType? {
      get {
        return (snapshot["createMessageType"] as? Snapshot).flatMap { CreateMessageType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createMessageType")
      }
    }

    public struct CreateMessageType: GraphQLSelectionSet {
      public static let possibleTypes = ["MessageType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("channelId", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("content", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, channelId: GraphQLID, content: String, type: Int, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "MessageType", "id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var channelId: GraphQLID {
        get {
          return snapshot["channelId"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "channelId")
        }
      }

      public var content: String {
        get {
          return snapshot["content"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "content")
        }
      }

      public var type: Int {
        get {
          return snapshot["type"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class UpdateMessageTypeMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateMessageType($input: UpdateMessageTypeInput!) {\n  updateMessageType(input: $input) {\n    __typename\n    id\n    channelId\n    content\n    type\n    createdAt\n    uuid\n  }\n}"

  public var input: UpdateMessageTypeInput

  public init(input: UpdateMessageTypeInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateMessageType", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateMessageType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateMessageType: UpdateMessageType? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateMessageType": updateMessageType.flatMap { $0.snapshot }])
    }

    public var updateMessageType: UpdateMessageType? {
      get {
        return (snapshot["updateMessageType"] as? Snapshot).flatMap { UpdateMessageType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateMessageType")
      }
    }

    public struct UpdateMessageType: GraphQLSelectionSet {
      public static let possibleTypes = ["MessageType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("channelId", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("content", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, channelId: GraphQLID, content: String, type: Int, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "MessageType", "id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var channelId: GraphQLID {
        get {
          return snapshot["channelId"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "channelId")
        }
      }

      public var content: String {
        get {
          return snapshot["content"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "content")
        }
      }

      public var type: Int {
        get {
          return snapshot["type"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class DeleteMessageTypeMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteMessageType($input: DeleteMessageTypeInput!) {\n  deleteMessageType(input: $input) {\n    __typename\n    id\n    channelId\n    content\n    type\n    createdAt\n    uuid\n  }\n}"

  public var input: DeleteMessageTypeInput

  public init(input: DeleteMessageTypeInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteMessageType", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteMessageType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteMessageType: DeleteMessageType? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteMessageType": deleteMessageType.flatMap { $0.snapshot }])
    }

    public var deleteMessageType: DeleteMessageType? {
      get {
        return (snapshot["deleteMessageType"] as? Snapshot).flatMap { DeleteMessageType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteMessageType")
      }
    }

    public struct DeleteMessageType: GraphQLSelectionSet {
      public static let possibleTypes = ["MessageType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("channelId", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("content", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, channelId: GraphQLID, content: String, type: Int, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "MessageType", "id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var channelId: GraphQLID {
        get {
          return snapshot["channelId"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "channelId")
        }
      }

      public var content: String {
        get {
          return snapshot["content"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "content")
        }
      }

      public var type: Int {
        get {
          return snapshot["type"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class CreateChannelTypeMutation: GraphQLMutation {
  public static let operationString =
    "mutation CreateChannelType($input: CreateChannelTypeInput!) {\n  createChannelType(input: $input) {\n    __typename\n    id\n    name\n    type\n    createdAt\n    uuid\n  }\n}"

  public var input: CreateChannelTypeInput

  public init(input: CreateChannelTypeInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createChannelType", arguments: ["input": GraphQLVariable("input")], type: .object(CreateChannelType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(createChannelType: CreateChannelType? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "createChannelType": createChannelType.flatMap { $0.snapshot }])
    }

    public var createChannelType: CreateChannelType? {
      get {
        return (snapshot["createChannelType"] as? Snapshot).flatMap { CreateChannelType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "createChannelType")
      }
    }

    public struct CreateChannelType: GraphQLSelectionSet {
      public static let possibleTypes = ["ChannelType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .scalar(Int.self)),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, name: String, type: Int? = nil, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "ChannelType", "id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return snapshot["name"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var type: Int? {
        get {
          return snapshot["type"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class UpdateChannelTypeMutation: GraphQLMutation {
  public static let operationString =
    "mutation UpdateChannelType($input: UpdateChannelTypeInput!) {\n  updateChannelType(input: $input) {\n    __typename\n    id\n    name\n    type\n    createdAt\n    uuid\n  }\n}"

  public var input: UpdateChannelTypeInput

  public init(input: UpdateChannelTypeInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("updateChannelType", arguments: ["input": GraphQLVariable("input")], type: .object(UpdateChannelType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(updateChannelType: UpdateChannelType? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "updateChannelType": updateChannelType.flatMap { $0.snapshot }])
    }

    public var updateChannelType: UpdateChannelType? {
      get {
        return (snapshot["updateChannelType"] as? Snapshot).flatMap { UpdateChannelType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "updateChannelType")
      }
    }

    public struct UpdateChannelType: GraphQLSelectionSet {
      public static let possibleTypes = ["ChannelType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .scalar(Int.self)),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, name: String, type: Int? = nil, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "ChannelType", "id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return snapshot["name"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var type: Int? {
        get {
          return snapshot["type"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class DeleteChannelTypeMutation: GraphQLMutation {
  public static let operationString =
    "mutation DeleteChannelType($input: DeleteChannelTypeInput!) {\n  deleteChannelType(input: $input) {\n    __typename\n    id\n    name\n    type\n    createdAt\n    uuid\n  }\n}"

  public var input: DeleteChannelTypeInput

  public init(input: DeleteChannelTypeInput) {
    self.input = input
  }

  public var variables: GraphQLMap? {
    return ["input": input]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("deleteChannelType", arguments: ["input": GraphQLVariable("input")], type: .object(DeleteChannelType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(deleteChannelType: DeleteChannelType? = nil) {
      self.init(snapshot: ["__typename": "Mutation", "deleteChannelType": deleteChannelType.flatMap { $0.snapshot }])
    }

    public var deleteChannelType: DeleteChannelType? {
      get {
        return (snapshot["deleteChannelType"] as? Snapshot).flatMap { DeleteChannelType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "deleteChannelType")
      }
    }

    public struct DeleteChannelType: GraphQLSelectionSet {
      public static let possibleTypes = ["ChannelType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .scalar(Int.self)),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, name: String, type: Int? = nil, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "ChannelType", "id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return snapshot["name"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var type: Int? {
        get {
          return snapshot["type"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class GetMessageTypeQuery: GraphQLQuery {
  public static let operationString =
    "query GetMessageType($id: ID!) {\n  getMessageType(id: $id) {\n    __typename\n    id\n    channelId\n    content\n    type\n    createdAt\n    uuid\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getMessageType", arguments: ["id": GraphQLVariable("id")], type: .object(GetMessageType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getMessageType: GetMessageType? = nil) {
      self.init(snapshot: ["__typename": "Query", "getMessageType": getMessageType.flatMap { $0.snapshot }])
    }

    public var getMessageType: GetMessageType? {
      get {
        return (snapshot["getMessageType"] as? Snapshot).flatMap { GetMessageType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getMessageType")
      }
    }

    public struct GetMessageType: GraphQLSelectionSet {
      public static let possibleTypes = ["MessageType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("channelId", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("content", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, channelId: GraphQLID, content: String, type: Int, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "MessageType", "id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var channelId: GraphQLID {
        get {
          return snapshot["channelId"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "channelId")
        }
      }

      public var content: String {
        get {
          return snapshot["content"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "content")
        }
      }

      public var type: Int {
        get {
          return snapshot["type"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class ListMessageTypesQuery: GraphQLQuery {
  public static let operationString =
    "query ListMessageTypes($filter: TableMessageTypeFilterInput, $channelId: String, $limit: Int, $nextToken: String) {\n  listMessageTypes(filter: $filter, channelId: $channelId, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      channelId\n      content\n      type\n      createdAt\n      uuid\n    }\n    nextToken\n  }\n}"

  public var filter: TableMessageTypeFilterInput?
  public var channelId: String?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: TableMessageTypeFilterInput? = nil, channelId: String? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.channelId = channelId
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "channelId": channelId, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listMessageTypes", arguments: ["filter": GraphQLVariable("filter"), "channelId": GraphQLVariable("channelId"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListMessageType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listMessageTypes: ListMessageType? = nil) {
      self.init(snapshot: ["__typename": "Query", "listMessageTypes": listMessageTypes.flatMap { $0.snapshot }])
    }

    public var listMessageTypes: ListMessageType? {
      get {
        return (snapshot["listMessageTypes"] as? Snapshot).flatMap { ListMessageType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listMessageTypes")
      }
    }

    public struct ListMessageType: GraphQLSelectionSet {
      public static let possibleTypes = ["MessageTypeConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "MessageTypeConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["MessageType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("channelId", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("content", type: .nonNull(.scalar(String.self))),
          GraphQLField("type", type: .nonNull(.scalar(Int.self))),
          GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
          GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, channelId: GraphQLID, content: String, type: Int, createdAt: Double, uuid: String) {
          self.init(snapshot: ["__typename": "MessageType", "id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var channelId: GraphQLID {
          get {
            return snapshot["channelId"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "channelId")
          }
        }

        public var content: String {
          get {
            return snapshot["content"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "content")
          }
        }

        public var type: Int {
          get {
            return snapshot["type"]! as! Int
          }
          set {
            snapshot.updateValue(newValue, forKey: "type")
          }
        }

        public var createdAt: Double {
          get {
            return snapshot["createdAt"]! as! Double
          }
          set {
            snapshot.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var uuid: String {
          get {
            return snapshot["uuid"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "uuid")
          }
        }
      }
    }
  }
}

public final class GetChannelTypeQuery: GraphQLQuery {
  public static let operationString =
    "query GetChannelType($id: ID!) {\n  getChannelType(id: $id) {\n    __typename\n    id\n    name\n    type\n    createdAt\n    uuid\n  }\n}"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("getChannelType", arguments: ["id": GraphQLVariable("id")], type: .object(GetChannelType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(getChannelType: GetChannelType? = nil) {
      self.init(snapshot: ["__typename": "Query", "getChannelType": getChannelType.flatMap { $0.snapshot }])
    }

    public var getChannelType: GetChannelType? {
      get {
        return (snapshot["getChannelType"] as? Snapshot).flatMap { GetChannelType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "getChannelType")
      }
    }

    public struct GetChannelType: GraphQLSelectionSet {
      public static let possibleTypes = ["ChannelType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .scalar(Int.self)),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, name: String, type: Int? = nil, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "ChannelType", "id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return snapshot["name"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var type: Int? {
        get {
          return snapshot["type"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class ListChannelTypesQuery: GraphQLQuery {
  public static let operationString =
    "query ListChannelTypes($filter: TableChannelTypeFilterInput, $limit: Int, $nextToken: String) {\n  listChannelTypes(filter: $filter, limit: $limit, nextToken: $nextToken) {\n    __typename\n    items {\n      __typename\n      id\n      name\n      type\n      createdAt\n      uuid\n    }\n    nextToken\n  }\n}"

  public var filter: TableChannelTypeFilterInput?
  public var limit: Int?
  public var nextToken: String?

  public init(filter: TableChannelTypeFilterInput? = nil, limit: Int? = nil, nextToken: String? = nil) {
    self.filter = filter
    self.limit = limit
    self.nextToken = nextToken
  }

  public var variables: GraphQLMap? {
    return ["filter": filter, "limit": limit, "nextToken": nextToken]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("listChannelTypes", arguments: ["filter": GraphQLVariable("filter"), "limit": GraphQLVariable("limit"), "nextToken": GraphQLVariable("nextToken")], type: .object(ListChannelType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(listChannelTypes: ListChannelType? = nil) {
      self.init(snapshot: ["__typename": "Query", "listChannelTypes": listChannelTypes.flatMap { $0.snapshot }])
    }

    public var listChannelTypes: ListChannelType? {
      get {
        return (snapshot["listChannelTypes"] as? Snapshot).flatMap { ListChannelType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "listChannelTypes")
      }
    }

    public struct ListChannelType: GraphQLSelectionSet {
      public static let possibleTypes = ["ChannelTypeConnection"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("items", type: .list(.object(Item.selections))),
        GraphQLField("nextToken", type: .scalar(String.self)),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(items: [Item?]? = nil, nextToken: String? = nil) {
        self.init(snapshot: ["__typename": "ChannelTypeConnection", "items": items.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, "nextToken": nextToken])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var items: [Item?]? {
        get {
          return (snapshot["items"] as? [Snapshot?]).flatMap { $0.map { $0.flatMap { Item(snapshot: $0) } } }
        }
        set {
          snapshot.updateValue(newValue.flatMap { $0.map { $0.flatMap { $0.snapshot } } }, forKey: "items")
        }
      }

      public var nextToken: String? {
        get {
          return snapshot["nextToken"] as? String
        }
        set {
          snapshot.updateValue(newValue, forKey: "nextToken")
        }
      }

      public struct Item: GraphQLSelectionSet {
        public static let possibleTypes = ["ChannelType"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("type", type: .scalar(Int.self)),
          GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
          GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        ]

        public var snapshot: Snapshot

        public init(snapshot: Snapshot) {
          self.snapshot = snapshot
        }

        public init(id: GraphQLID, name: String, type: Int? = nil, createdAt: Double, uuid: String) {
          self.init(snapshot: ["__typename": "ChannelType", "id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid])
        }

        public var __typename: String {
          get {
            return snapshot["__typename"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return snapshot["id"]! as! GraphQLID
          }
          set {
            snapshot.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String {
          get {
            return snapshot["name"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "name")
          }
        }

        public var type: Int? {
          get {
            return snapshot["type"] as? Int
          }
          set {
            snapshot.updateValue(newValue, forKey: "type")
          }
        }

        public var createdAt: Double {
          get {
            return snapshot["createdAt"]! as! Double
          }
          set {
            snapshot.updateValue(newValue, forKey: "createdAt")
          }
        }

        public var uuid: String {
          get {
            return snapshot["uuid"]! as! String
          }
          set {
            snapshot.updateValue(newValue, forKey: "uuid")
          }
        }
      }
    }
  }
}

public final class OnCreateMessageTypeSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateMessageType {\n  onCreateMessageType {\n    __typename\n    id\n    channelId\n    content\n    type\n    createdAt\n    uuid\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateMessageType", type: .object(OnCreateMessageType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateMessageType: OnCreateMessageType? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateMessageType": onCreateMessageType.flatMap { $0.snapshot }])
    }

    public var onCreateMessageType: OnCreateMessageType? {
      get {
        return (snapshot["onCreateMessageType"] as? Snapshot).flatMap { OnCreateMessageType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateMessageType")
      }
    }

    public struct OnCreateMessageType: GraphQLSelectionSet {
      public static let possibleTypes = ["MessageType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("channelId", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("content", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, channelId: GraphQLID, content: String, type: Int, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "MessageType", "id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var channelId: GraphQLID {
        get {
          return snapshot["channelId"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "channelId")
        }
      }

      public var content: String {
        get {
          return snapshot["content"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "content")
        }
      }

      public var type: Int {
        get {
          return snapshot["type"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class OnUpdateMessageTypeSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateMessageType {\n  onUpdateMessageType {\n    __typename\n    id\n    channelId\n    content\n    type\n    createdAt\n    uuid\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateMessageType", type: .object(OnUpdateMessageType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateMessageType: OnUpdateMessageType? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateMessageType": onUpdateMessageType.flatMap { $0.snapshot }])
    }

    public var onUpdateMessageType: OnUpdateMessageType? {
      get {
        return (snapshot["onUpdateMessageType"] as? Snapshot).flatMap { OnUpdateMessageType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateMessageType")
      }
    }

    public struct OnUpdateMessageType: GraphQLSelectionSet {
      public static let possibleTypes = ["MessageType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("channelId", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("content", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, channelId: GraphQLID, content: String, type: Int, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "MessageType", "id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var channelId: GraphQLID {
        get {
          return snapshot["channelId"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "channelId")
        }
      }

      public var content: String {
        get {
          return snapshot["content"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "content")
        }
      }

      public var type: Int {
        get {
          return snapshot["type"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class OnDeleteMessageTypeSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteMessageType {\n  onDeleteMessageType {\n    __typename\n    id\n    channelId\n    content\n    type\n    createdAt\n    uuid\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteMessageType", type: .object(OnDeleteMessageType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteMessageType: OnDeleteMessageType? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteMessageType": onDeleteMessageType.flatMap { $0.snapshot }])
    }

    public var onDeleteMessageType: OnDeleteMessageType? {
      get {
        return (snapshot["onDeleteMessageType"] as? Snapshot).flatMap { OnDeleteMessageType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteMessageType")
      }
    }

    public struct OnDeleteMessageType: GraphQLSelectionSet {
      public static let possibleTypes = ["MessageType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("channelId", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("content", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(Int.self))),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, channelId: GraphQLID, content: String, type: Int, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "MessageType", "id": id, "channelId": channelId, "content": content, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var channelId: GraphQLID {
        get {
          return snapshot["channelId"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "channelId")
        }
      }

      public var content: String {
        get {
          return snapshot["content"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "content")
        }
      }

      public var type: Int {
        get {
          return snapshot["type"]! as! Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class OnCreateChannelTypeSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnCreateChannelType {\n  onCreateChannelType {\n    __typename\n    id\n    name\n    type\n    createdAt\n    uuid\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onCreateChannelType", type: .object(OnCreateChannelType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onCreateChannelType: OnCreateChannelType? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onCreateChannelType": onCreateChannelType.flatMap { $0.snapshot }])
    }

    public var onCreateChannelType: OnCreateChannelType? {
      get {
        return (snapshot["onCreateChannelType"] as? Snapshot).flatMap { OnCreateChannelType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onCreateChannelType")
      }
    }

    public struct OnCreateChannelType: GraphQLSelectionSet {
      public static let possibleTypes = ["ChannelType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .scalar(Int.self)),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, name: String, type: Int? = nil, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "ChannelType", "id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return snapshot["name"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var type: Int? {
        get {
          return snapshot["type"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class OnUpdateChannelTypeSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnUpdateChannelType {\n  onUpdateChannelType {\n    __typename\n    id\n    name\n    type\n    createdAt\n    uuid\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onUpdateChannelType", type: .object(OnUpdateChannelType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onUpdateChannelType: OnUpdateChannelType? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onUpdateChannelType": onUpdateChannelType.flatMap { $0.snapshot }])
    }

    public var onUpdateChannelType: OnUpdateChannelType? {
      get {
        return (snapshot["onUpdateChannelType"] as? Snapshot).flatMap { OnUpdateChannelType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onUpdateChannelType")
      }
    }

    public struct OnUpdateChannelType: GraphQLSelectionSet {
      public static let possibleTypes = ["ChannelType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .scalar(Int.self)),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, name: String, type: Int? = nil, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "ChannelType", "id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return snapshot["name"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var type: Int? {
        get {
          return snapshot["type"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}

public final class OnDeleteChannelTypeSubscription: GraphQLSubscription {
  public static let operationString =
    "subscription OnDeleteChannelType {\n  onDeleteChannelType {\n    __typename\n    id\n    name\n    type\n    createdAt\n    uuid\n  }\n}"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Subscription"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("onDeleteChannelType", type: .object(OnDeleteChannelType.selections)),
    ]

    public var snapshot: Snapshot

    public init(snapshot: Snapshot) {
      self.snapshot = snapshot
    }

    public init(onDeleteChannelType: OnDeleteChannelType? = nil) {
      self.init(snapshot: ["__typename": "Subscription", "onDeleteChannelType": onDeleteChannelType.flatMap { $0.snapshot }])
    }

    public var onDeleteChannelType: OnDeleteChannelType? {
      get {
        return (snapshot["onDeleteChannelType"] as? Snapshot).flatMap { OnDeleteChannelType(snapshot: $0) }
      }
      set {
        snapshot.updateValue(newValue?.snapshot, forKey: "onDeleteChannelType")
      }
    }

    public struct OnDeleteChannelType: GraphQLSelectionSet {
      public static let possibleTypes = ["ChannelType"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .scalar(Int.self)),
        GraphQLField("createdAt", type: .nonNull(.scalar(Double.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
      ]

      public var snapshot: Snapshot

      public init(snapshot: Snapshot) {
        self.snapshot = snapshot
      }

      public init(id: GraphQLID, name: String, type: Int? = nil, createdAt: Double, uuid: String) {
        self.init(snapshot: ["__typename": "ChannelType", "id": id, "name": name, "type": type, "createdAt": createdAt, "uuid": uuid])
      }

      public var __typename: String {
        get {
          return snapshot["__typename"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return snapshot["id"]! as! GraphQLID
        }
        set {
          snapshot.updateValue(newValue, forKey: "id")
        }
      }

      public var name: String {
        get {
          return snapshot["name"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "name")
        }
      }

      public var type: Int? {
        get {
          return snapshot["type"] as? Int
        }
        set {
          snapshot.updateValue(newValue, forKey: "type")
        }
      }

      public var createdAt: Double {
        get {
          return snapshot["createdAt"]! as! Double
        }
        set {
          snapshot.updateValue(newValue, forKey: "createdAt")
        }
      }

      public var uuid: String {
        get {
          return snapshot["uuid"]! as! String
        }
        set {
          snapshot.updateValue(newValue, forKey: "uuid")
        }
      }
    }
  }
}